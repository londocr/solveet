package com.ciriscr.desafios

import org.scalatest.FunSuite
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 19/07/12
 * Time: 09:22 PM
 */

class TestSimpleEficiente extends FunSuite with SimpleEficiente {

  test("Eficiente") {
    val nums = List(3, 7, 12)
    for (n <- nums){
      val ini = new Date().getTime
      f(n, 0, n+1)
      val fin = new Date().getTime
      println(fin - ini)
    }
  }

}
