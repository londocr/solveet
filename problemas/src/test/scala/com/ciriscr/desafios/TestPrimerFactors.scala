package com.ciriscr.desafios

import org.scalatest.FunSuite
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 25/07/12
 * Time: 11:42 AM
 */

class TestPrimerFactors extends FunSuite with PrimerFactors {

  test("PrimerFactors"){
    println("comienzo")
    for (n <- 2 to 1249812094; if n % 2 == 1) {
//    for (n <- 2 to 12498; if n % 2 == 1)
      if (n % 1000000 == 5) println(n + " " + new Date().toString)
      assert(n === getFactors(n).fold(1)(_ * _))
    }
  }

}
