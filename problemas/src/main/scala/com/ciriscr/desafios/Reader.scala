package com.ciriscr.desafios

import io.Source

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 25/09/12
 * Time: 02:05 PM
 */

class Reader(filePath: String) {
  def read:Iterator[String] = Source.fromFile(filePath).getLines()
}
