package com.ciriscr.desafios

import java.io.File

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 23/07/12
 * Time: 08:25 PM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait FileListing {

  def printDirectories(f: File)(depth: Int): Int = {
    val a = for (d <- f.listFiles()
                 if d.isDirectory) yield {
      printDir(d, depth)
      printDirectories(d)(depth + 1)
    }
    a.fold(1)(_ + _)
  }

  private def printDir(f: File, depth: Int) {
    println(Array.fill[String](depth)("--").mkString + f.getName)
  }

}
