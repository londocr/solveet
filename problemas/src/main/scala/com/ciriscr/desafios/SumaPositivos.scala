package com.ciriscr.desafios

import io.Source

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 01/06/12
 * Time: 10:00 PM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait SumaPositivos {

  def sumar(file: String) = {
    val nums = new Reader(file).read.toList.drop(1).map(_.toInt)
    nums.filter(_ > 0).fold(0)(_ + _)
  }

}
