package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 01/06/12
 * Time: 10:25 PM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait LagartoSpock {

  val elem = List(new Elemento("Tijeras", List("Lagarto", "Papel"), List("Spock", "Piedra")),
                  new Elemento("Lagarto", List("Spock", "Papel"), List("Tijeras", "Piedra")),
                  new Elemento("Papel", List("Spock", "Piedra"), List("Lagarto", "Tijeras")),
                  new Elemento("Spock", List("Tijeras", "Piedra"), List("Lagarto", "Papel")),
                  new Elemento("Piedra", List("Lagarto", "Tijeras"), List("Spock", "Papel"))
                 )

  def validarJuego(c: (String, String)) = elem.find(_.nombre == c._1).get.validarTurno(c._2)

}

class Elemento(val nombre: String, val fortaleza: List[String], val debilidades: List[String]){
  def validarTurno(contrincante: String) = {
    if (nombre == contrincante) Resultado.Empate
    else {
      if (fortaleza.contains(contrincante)) Resultado.Gana1 else Resultado.Gana2
    }
  }
}

object Resultado extends Enumeration {
  val Empate = "Empate"
  val Gana1 = "Gana jugador 1"
  val Gana2 = "Gana jugador 2"
}
